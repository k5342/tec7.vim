#tec.vim
the TeC syntax for vim

##Feature
 * Support TeC7 syntax
 
 **TODO:** Support neocomplete.vim

##Download

###NeoBundle
write your ~/.vimrc

```
#!vim
NeoBundle 'https://bitbucket.org/k5342/tec7.vim'
```

###Manual
download or clone the repository.
and, put tec.vim into ~/.vim/syntax directory

##Install
write your ~/.vimrc or ~/.vim/filetype.vim

```
#!vim
au BufNewFile,BufRead *.t7  setlocal tabstop=8 shiftwidth=8 filetype=tec7
```

(change the extension depending on the environment in use)
