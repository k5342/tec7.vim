" ----------------------------------------------------
" Vim syntax file
" Language:            TeC7 Assembler(Version 7+)
" Maintainer:          ks
" Last Modified:       2014/01/17
" ----------------------------------------------------

if version < 600
  syntax clear
elseif exists( "b:current_syntax" )
  finish
endif

if version < 508
  command! -nargs=+ TeC7HiLink hi link <args>
else
  command! -nargs=+ TeC7HiLink hi def link <args>
endif

TeC7HiLink   TeC7String       String
TeC7HiLink   TeC7Character    Character
TeC7HiLink   TeC7Pointer      Number
TeC7HiLink   TeC7Number       Number
TeC7HiLink   TeC7Keyword      Keyword
TeC7HiLink   TeC7Conditional  Conditional
TeC7HiLink   TeC7Register     Identifier
TeC7HiLink   TeC7Comment      Comment
TeC7HiLink   TeC7Define       Define

syn region   TeC7String       start=+"+ end=+"+

syn match    TeC7Character    /'.'/
syn match    TeC7Pointer      /#[A-Za-z0-9]\+/
syn match    TeC7Number       /[-+#][0-9a-fA-F]\+\(H\|\)/
syn match    TeC7Comment      /;.*$/
syn keyword  TeC7Keyword      NO LD ST ADD SUB CMP AND OR XOR
syn keyword  TeC7Keyword      SHLA SHLL SHRA SHRL IN OUT CALL
syn keyword  TeC7Keyword      PUSH POP PUSHF POPF EI DI RET RETI
syn keyword  TeC7Conditional  JZ JM JC JNZ JNM JNC JMP HALT
syn keyword  TeC7Register     G0 G1 G2 SP
syn keyword  TeC7Define       DC DS
